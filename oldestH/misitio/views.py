from django.shortcuts import render
from django.contrib.auth.models import User
from misitio.models import Perfil_Usuario
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as do_logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def index (request):
    return render(request,'misitio/index.html',{})

def logear (request):
    return render(request,'misitio/Login.html',{})

def servicios (request):
    return render(request,'misitio/servicios.html',{})

def fotos (request):
    return render(request,'misitio/fotos.html',{})

def contacto (request):
    return render(request,'misitio/contacto.html',{}) 

def registro (request):
    return render(request,'misitio/registro.html',{})   

def noRegistrado (request):
    return render(request, 'misitio/noRegistrado.html',{})

def perfil (request):
    return render(request,'misitio/perfil.html',{})

def config (request):
    return render(request,'misitio/config.html',{})    

def indexL (request):
    return render(request,'misitio/indexL.html')

def contactoL (request):
    return render(request,'misitio/contactoL.html')

def serviciosL (request):
    return render(request,'misitio/serviciosL.html')


# metodo para registrar usuarios tomando los datos que se solicitan
def formRegistro (request):
    if request.method == 'POST':
        user = User.objects.create_user(request.POST.get(
            'correo'), request.POST.get('correo'), request.POST.get('contrasena'))

        perfil = Perfil_Usuario()

        perfil.User = user
        perfil.nombreIngreso = request.POST.get('nombreIngreso')
        perfil.apellidosIngreso = request.POST.get('apellidosIngreso')
        perfil.RutIngreso = request.POST.get('RutIngreso')
        perfil.Edad = request.POST.get('Edad')

        perfil.save()

    return (HttpResponseRedirect('/')) 

# metodo para logearse y redireccionar en caso de que el usuario no este registro
def logn (request): 
    if request.method == 'POST':
        usuario = request.POST.get('userL')
        contra = request.POST.get('passwordL')
        user = authenticate(request,username=usuario, password=contra)
        if user is not None:
            # si él usuario está en la bd se le redireccionará al perfil
            login (request,user)
            return (HttpResponseRedirect('/diccionario/'))
        else:
            # si él usuario no está en la bd se le redireccionará a la pagina de noRegistrado
            return(HttpResponseRedirect('noRegistrado'))
    else:
            return (HttpResponse('404 Not Found'))


@login_required
def logout(request):
    do_logout(request)
    return(HttpResponseRedirect('/'))


#diccionario de datos
@login_required
def diccionario(request):
    usuario =  request.user
    perfilUsuario = Perfil_Usuario.objects.get(User = usuario)
    datos_perfil = {'User': perfilUsuario.User, 'nombre': perfilUsuario.nombreIngreso, 'apellido': perfilUsuario.apellidosIngreso,
                              'rut': perfilUsuario.RutIngreso, 'edad':perfilUsuario.Edad, 'avatar': perfilUsuario.foto_perfil}
    return render (request, 'misitio/perfil.html', datos_perfil)

@login_required
def update(request):
    if request.method == 'POST':
        usuario = request.user
        perfilUsuario = Perfil_Usuario()

        nomuser = Perfil_Usuario.objects.get(User = usuario)
        perfil = Perfil_Usuario.objects.get(pk = nomuser)

        perfil.nombreIngreso = request.POST.get('nombreIngreso')
        perfil.apellidosIngreso = request.POST.get('apellidosIngreso')
        perfil.foto_perfil = request.FILES.get('foto_perfil')


        if request.FILES.get('imagen'):
            perfil= request.user
            file = request.FILES.get('imagen')
            perfil.foto_perfil = file

        perfil.save()   

        return HttpResponseRedirect ('/diccionario/')

@login_required
def delete(request):
    if request.method =='POST':
        perfil= request.user
        perfil.delete()
        return(HttpResponseRedirect('/'))
