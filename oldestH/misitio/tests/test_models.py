import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestPerfilUsuario:

    def test_model(self):
        obj = mixer.blend('misitio.Perfil_Usuario')
        assert obj.pk == 1, 'Mensaje de error?'

    def test_str(self):
        nombreIngreso = "Nombre Prueba"
        obj = mixer.blend('misitio.Perfil_Usuario', nombreIngreso=nombreIngreso)

        assert str(obj) == nombreIngreso

    def test_create(self):
        obj = mixer.blend('misitio.Perfil_Usuario')
        assert obj.create(obj.User) == True

    def test_modificar(self):
        nombre_modificado = 'Nombre modificado'
        obj = mixer.blend('misitio.Perfil_Usuario')

        obj.nombreIngreso = nombre_modificado

        assert obj.modificar() == True
