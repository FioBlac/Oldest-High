$(document).ready(function(){
    var validacionUno = 1;
    var validacionDos = 1;

    $('#email').keyup(function(){  /* Validacion Email*/
        var largo = $(this).val().length;
        if(largo==0){
            estiloValidar(this,2);
            validacionUno = 1;
        }else{
            if(largo<5){
            estiloValidar(this,0);
            validacionUno = 1;
            }else{
                estiloValidar(this,1);
                validacionUno = 0;
            }
        }
    });
    $('#pass, #pass1').keyup(function(){  /* Validacion password*/
        var largo = $(this).val().length;
        if(largo==0){
            estiloValidar(this,2);
            validacionDos = 1;
        }else{
            if(largo<8){
            estiloValidar(this,0);
            validacionDos = 1;
            }else{
                estiloValidar(this,1);
                validacionDos = 0;
            }
        }
    });
    $('#sub').click(function() {
        if(validacionUno+validacionDos==0){
            window.location.href = 'index.html';
        }
    });

    $('#RutIngreso, #rut').keyup(function(){  /* Validacion largo del rut*/
        var largo = $(this).val().length;
        if(largo<=9){
            estiloValidar(this,0);
            validacionDos = 1;
        }else{
            if(largo==10){
            estiloValidar(this,1);
            validacionDos = 1;
            }else{
                estiloValidar(this,1);
                validacionDos = 0;
            }
        }
    });


    $('#sub').click(function() {
        if(validacionUno+validacionDos==0){
            window.location.href = 'index.html';
        }
    });


    $('#pass2').keyup(function() {
        var largo = $(this).val().length;
		var pass1 = $('#pass1').val();
		var pass2 = $('#pass2').val();

		if ( pass1 == pass2 ) {
			estiloValidar(this,1);
		} else {
			estiloValidar(this,0);
		}if(largo==0){
            estiloValidar(this,2); 
        }

	});
});

function full_letras(e) {  /* VALIDAR PARA QUE EN EL NOMBRE Y APELLIDO SOLO SE INGRESEN LETRAS */
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function letraK(e) {    /* VALIDAR PARA QUE EN EL RUT SOLO SE PRECIONE LA K */
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789-k";
    especiales = "8-37-39-46-";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function estiloValidar(elemento, valido){
    switch(valido) {
        case 1:
            $(elemento).css("border-bottom", "1px solid #34ff00");/* Valido */
          break;
        case 0:
            $(elemento).css("border-bottom", "1px solid #ff0000");/* No Valido*/
          break;
        default:
            $(elemento).css("border-bottom", "none");/* Vacio */
      }
}