from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('logear', views.logear, name='logear'),
    path('servicios', views.servicios, name='servicios'),
    path('fotos', views.fotos, name='fotos'),
    path('contacto', views.contacto, name='contacto'),
    path('registro', views.registro, name='registro'),
    path('formRegistro/', views.formRegistro, name='formRegistro'),
    path('logn', views.logn, name='logn',),
    path('noRegistrado', views.noRegistrado, name='noRegistrado'),
    path('perfil', views.perfil, name='perfil'),
    path('update',views.update, name='update'),
    path('delete/',views.delete, name='delete'),
    path('logout',views.logout, name='logout'),
    path('diccionario/', views.diccionario, name='diccionario'),
    path('indexL', views.indexL, name='indexL'),
    path('contactoL', views.contactoL, name='contactoL'),
    path('serviciosL', views.serviciosL, name='serviciosL')
]