from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Perfil_Usuario(models.Model):
    User = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True) 
    nombreIngreso = models.CharField(max_length=150)
    apellidosIngreso = models.CharField(max_length=150)
    RutIngreso = models.CharField(max_length=10)
    Edad = models.IntegerField(default=18)
    foto_perfil = models.ImageField(upload_to="fotoPerfil", null=True)
    # foto_perfil = models.FileField(null = True, blank= True)

    def __str__(self):
        return self.nombreIngreso

    def create(self, user):
        self.User = user

        try:
            self.save()
            return True
        except:
            return False

    def modificar(self):
        try:
            self.save()
            return True
        except:
            return False
